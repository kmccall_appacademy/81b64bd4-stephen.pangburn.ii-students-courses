class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first, last)
    @first_name = first
    @last_name = last
    @courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end

  def enroll(course)
    raise "There is a course conflict" if has_conflict?(course)
    unless @courses.include?(course)
      @courses << course
      course.students << self
    end
  end

  def has_conflict?(new_course)
    @courses.any? do |course|
      course.conflicts_with?(new_course)
    end
  end

  def course_load
    credits = Hash.new(0)

    @courses.each do |course|
      credits[course.department] += course.credits
    end

    credits
  end
end
